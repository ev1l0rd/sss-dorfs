###   `Zewia', Engraver  
 
***

He feels euphoric due to inebriation.  He is blissful after sleeping in a very good bedroom.  Within the last season, he was annoyed after having a drink without using a goblet, cup or mug.  He was irritated when thirsty.  He felt bitter after getting into an argument.  He was irritated when drowsy.  He felt euphoric due to inebriation.  He was annoyed at the lack of chairs.  He felt satisfied at work.  He felt satisfied at work.  He felt fondness talking with a friend.  
***

He is romantically involved with `pbanj'.  He is a worshipper of Datan.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

He is eighty-one years old, born on the 21st of Opal in the year 431.  
***


***

His very long sideburns are braided.  His very long moustache is neatly combed.  His very long beard is braided.  His medium-length hair is neatly combed.  His round protruding ochre eyes are wide-set.  His lips are thick.  He has a narrow chin.  He has a scratchy voice.  His nose bridge is somewhat concave.  His eyebrows are quite long.  His eyes have slightly thin irises.  His ears are somewhat broad.  His sepia skin is slightly wrinkled.  His hair is burnt sienna with a touch of gray.  
***


***

`Zewia' likes kaolinite, aluminum, sunstone, green glass, slabs, rings, the sound of The Fancy Periwinkle and the sight of The Sienna Saturninity.  When possible, he prefers to consume herring, cassava beer and hemp flour.  He absolutely detests toads.  
***

He has a great feel for the surrounding space, a natural ability with music and a lot of willpower, but he has bad intuition, poor analytical abilities, a little difficulty with words and a very bad sense of empathy.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally dislikes cooperation and finds friendship burdensome.  He dreams of raising a family.  
***

He is very curious, sometimes to his detriment.  He is very polite and observes appropriate rules of decorum when possible.  He  is currently more rude.  He does not generally seek retribution for past wrongs.  He feels a strong need to reciprocate any favor done for him.  He has a calm demeanor.  He is a friendly individual, and he is burdened by this tendency because he dislikes the idea of friendship.  He generally acts with a narrow focus on the current activity.  He has a sense of duty.  He doesn't mind wearing something special now and again.  He is not particularly interested in what others think of him.  He  is currently more shameless.  He doesn't often experience strong cravings or urges.  He is brave in the face of imminent danger.  He  is currently more fearless.  He has a tendency to go it alone, without considering the advice of others.  He does not often feel lustful.  He has an active imagination.  He is moved by art and natural beauty, and he is troubled by this since he dislikes the natural world.  He can handle stress.  He  is currently more confident.  He  is currently less private.  He  is currently more thoughtless.  He has a menacing stare when he's angry.  He needs alcohol to get through the working day.  He likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, Alåth is unfocused by unmet needs.  He is not distracted after being away from people.  He is not distracted after being unoccupied.  He is not distracted after doing nothing creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is unfocused after being unable to fight.  He is level-headed after causing trouble.  He is unfocused after being unable to be extravagant.  He is unfocused after not learning anything.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is unfocused after being unable to admire art.  He is unfocused after being unable to practice a craft.  He is unfocused after being away from family.  He is unfocused after being unable to practice a martial art.  He is not distracted after being unable to practice a skill.  He is unfocused after being unable to take it easy.  He is unfettered after drinking.  He is unfocused after being unable to pray to Datan.  
***

A short, sturdy creature fond of drink and industry. 
***

