###   `Sonlen', Blacksmith  
 
***

"You can be powerful or powerless.  The choice is yours, until somebody makes it for you."
***

She is married to `Byokugen' and has 4 children:  Rîsen Merchantsplattered, Monom Handlelistened, Solon Smithmurdered and Mafol Veiledchamber.  She is the daughter of Solon Tempestpaint and Kadol Learnstake.  She is a casual worshipper of Datan, a worshipper of Togal the Strike of Turquoise, a faithful worshipper of The Chances of Coincidence, an ardent worshipper of Uker Moistensplashed the Oceanic Seal, a worshipper of Aned the Jewels of Chancing, a worshipper of Ral the Diamond Guilds and a casual worshipper of Ipa Fordlark the Mountainous Bear.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Humid Lantern.  She arrived at Titthalbomrek on the 7th of Malachite in the year 512.  
***

She is one hundred thirty-six years old, born on the 20th of Galena in the year 376.  
***

Her free-lobed ears are very splayed out.  She has a low voice.  Her nose is upturned.  Her medium-length hair is arranged in double braids.  Her hair is gray with some white.  Her skin is sepia.  Her eyes are ochre.  
***

She is rarely sick, but she is quick to tire and weak.  
***

`Sonlen' likes obsidian, zinc, sapphire, kiwi leather, adder tooth, amber, the color ochre, flasks, yaks for their shaggy hair and the sound of The Fancy Periwinkle.  When possible, she prefers to consume bloated tubers and hard wheat beer.  She absolutely detests brown recluse spiders.  
***

She has a feel for music, but she has a meager ability with social relationships, a meager kinesthetic sense, quite poor focus and very little linguistic ability.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally sees power over others as something to strive for and finds those that deny their impulses somewhat stiff.  She dreams of mastering a skill.  
***

She does not find most jokes humorous.  She finds helping others very emotionally rewarding.  She is quite ambitious.  She tries to do things correctly each time.  She is often cheerful.  She doesn't handle stress well.  She tends to ask others for help with difficult decisions.  She enjoys the company of others.  She thinks she is fairly important in the grand scheme of things.  She needs alcohol to get through the working day.  
***

Overall, Sigun is unfocused by unmet needs.  She is not distracted after being unable to pray to Togal the Strike of Turquoise.  She is not distracted after being unable to pray to The Chances of Coincidence.  She is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  She is not distracted after being unable to pray to Aned the Jewels of Chancing.  She is not distracted after being unable to pray to Ral the Diamond Guilds.  She is not distracted after being away from people.  She is not distracted after being unoccupied.  She is not distracted after doing nothing creative.  She is not distracted after leading an unexciting life.  She is not distracted after being unable to acquire something.  She is not distracted after being kept from alcohol.  She is not distracted after a lack of decent meals.  She is not distracted after being unable to fight.  She is not distracted after a lack of trouble-making.  She is not distracted after being unable to argue.  She is not distracted after being unable to be extravagant.  She is not distracted after not learning anything.  She is not distracted after being unable to help anybody.  She is not distracted after a lack of abstract thinking.  She is not distracted after being unable to make merry.  She is not distracted after being unable to admire art.  She is not distracted after being unable to practice a craft.  She is not distracted after being away from family.  She is not distracted after being away from friends.  She is not distracted after being unable to practice a martial art.  She is not distracted after being unable to practice a skill.  She is not distracted after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

###   `Sentry', Lye Maker  
 
***

"I'm well."
***

She is married to `kenn' and has three children:  Lokum Ragclan, Tun Channelgullies and Sigun Singeddiamonds.  She is the daughter of Vabôk Shottalons and Mistêm Rhythmblockades.  She is a dubious worshipper of Datan.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Humid Lantern.  She arrived at Titthalbomrek on the 7th of Malachite in the year 512.  
***

She is eighty-one years old, born on the 13th of Sandstone in the year 431.  
***

Her hair is dry.  Her long hair is tied in a pony tail.  Her eyebrows are quite long.  Her slightly close-set ochre eyes are slightly rounded.  Her hair is burnt sienna with a touch of gray.  Her skin is sepia.  Her nose is somewhat narrow.  
***

She is rarely sick.  
***

`Sentry' likes native gold, pig iron, red grossular, two-humped camel leather, sheep horn, linen fabric, the color ochre, battle axes, armor stands, giant leopard seals for their fierce nature and the words of The South Ways.  When possible, she prefers to consume nautilus, one-humped camel cheese, bloated tubers and gutter cruor.  She absolutely detests mosquitos.  
***

She has great creativity, a feel for music and the ability to focus, but she has a questionable spatial sense and bad intuition.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce and values knowledge.  She personally finds artwork boring and doesn't care about nature one way or another.  She dreams of crafting a masterwork someday.  
***

She is trusting.  She tends not to reveal personal information.  She does not easily hate or develop negative feelings.  She is pleased by her own appearance and talents.  She doesn't often experience strong cravings or urges.  She is grateful when others help her out and tries to return favors.  She does not easily fall in love and rarely develops positive sentiments.  She could be considered rude.  She tends to ask others for help with difficult decisions.  She is somewhat uncomfortable around those that appear unusual or live differently from herself.  She often feels discouraged.  She can handle stress.  She is quite ambitious.  She  tenses up when she's nervous.  She needs alcohol to get through the working day.  
***

Overall, Urdim is untroubled by unmet needs.  She is not distracted after being away from people.  She is not distracted after being unoccupied.  She is not distracted after doing nothing creative.  She is not distracted after leading an unexciting life.  She is not distracted after being unable to acquire something. She is not distracted after being unable to fight.  She is not distracted after a lack of trouble-making.  She is not distracted after being unable to argue.  She is not distracted after being unable to be extravagant.  She is not distracted after not learning anything.  She is not distracted after being unable to help anybody.  She is not distracted after a lack of abstract thinking.  She is not distracted after being unable to make merry.  She is not distracted after being unable to practice a craft.  She is not distracted after being away from family.  She is not distracted after being away from friends.  She is not distracted after being unable to practice a martial art.  She is not distracted after being unable to practice a skill.  She is not distracted after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

