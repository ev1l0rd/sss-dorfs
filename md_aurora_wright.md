###   `Aurora Wright', Thresher  
 
***

"I'm doing alright."Within the last season, she felt pleasure near a completely sublime Bridge.  She felt euphoric due to inebriation.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt pleasure near a fine Trap.  She felt pleasure near a fine Trap.  She felt pleasure near a fine Trap.  She was annoyed after sleeping on a rough cave floor.  She felt satisfied at work.  She was annoyed at the lack of chairs.  She was aroused talking with the spouse.  She felt satisfied at work.  She felt pleasure near a very fine Trap.  
***

She is married to `Lily'.  She is the daughter of Logem Cryptsubmerged and Kol Dreamswhip.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Vault of Tempests.  She arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

She is twenty-nine years old, born on the 2nd of Opal in the year 483.  
***

Her sideburns are clean-shaven.  Her very long moustache is arranged in double braids.  Her very long beard is braided.  Her very long hair is tied in a pony tail.  Her nose bridge is very convex.  She has a broad chin.  Her lips are thick.  Her somewhat splayed out tall ears are free-lobed.  Her sepia skin is slightly wrinkled.  Her hair is burnt sienna.  Her eyes are ochre.  
***

She is mighty, but she is flimsy.  
***

`Aurora Wright' likes magnetite, pig iron, pineapple opal, breastplates, bins, goblets, giant flying squirrels for their gliding and the words of The Flutes of Bewildering.  When possible, she prefers to consume garden cress and strawberry wine.  She absolutely detests flies.  
***

She has a great deal of patience, very good creativity and the ability to focus, but she has little linguistic ability.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally doesn't particularly value loyalty.  He dreams of mastering a skill.  
***

He is incurious and never seeks out knowledge or information to satisfy himself.  He is very stubborn.  He is not the type to fall in love or even develop positive feelings.  He generally acts impartially and is rarely moved to mercy.  He could be considered rude.  He  is currently more rude.  He tends to avoid crowds.  He tends to avoid any physical confrontations, and he works to square this natural tendency with his respect of martial prowess.  He doesn't focus on material goods.  He  is currently more fearless.  He  is currently more confident.  He  is currently more shameless.  He  is currently less private.  He  is currently more thoughtless.  She needs alcohol to get through the working day.  
***

Overall, Eral is unfocused by unmet needs.  She is not distracted after being unoccupied.  She is not distracted after doing nothing creative.  She is unfocused after leading an unexciting life.  She is level-headed after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after a lack of trouble-making.  She is unfocused after being unable to argue.  She is unfocused after being unable to be extravagant.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfettered after admiring art.  She is not distracted after being unable to practice a craft.  She is unfocused after not learning anything.  She is not distracted after being away from family.  She is unfocused after being away from friends.  She is unfocused after being unable to practice a martial art.  She is not distracted after being unable to practice a skill.  She is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

