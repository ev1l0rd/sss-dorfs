###   `Byokugen', Gem Setter  
 
***

"An artisan, their materials and the tools to shape them!"
***

He is married to `Sonlen' and has 4 children:  Rîsen Merchantsplattered, Monom Handlelistened, Solon Smithmurdered and Mafol Veiledchamber.  He is the son of Bëmbul Holysyrups and Litast Wheelssunken.  He is a dubious worshipper of Datan, a faithful worshipper of Togal the Strike of Turquoise, a worshipper of Aned the Jewels of Chancing, a worshipper of Uker Moistensplashed the Oceanic Seal and a worshipper of Ral the Diamond Guilds.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He is a former member of The Safe Glaze.  He is a former member of The Humid Lantern.  He arrived at Titthalbomrek on the 7th of Malachite in the year 512.  
***

He is one hundred forty-four years old, born on the 2nd of Galena in the year 368.  
***

He has very high cheekbones. His sideburns are clean-shaven.  His long moustache is neatly combed.  His very long beard is arranged in double braids.  His hair is clean-shaven.  His hair is white mixed with gray.  His skin is sepia.  His eyes are ochre.  
***

He is very rarely sick.  
***

`Byokugen' likes andesite, rose gold, honey yellow beryl, leather armor, giant otters for their playfulness and the words of The Flutes of Bewildering.  When possible, he prefers to consume wombat and papaya wine.  He absolutely detests toads.  
***

He has great analytical abilities, great intuition, a very good feel for social relationships and a good kinesthetic sense, but he has a large deficit of willpower, little patience and very little linguistic ability.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally strongly values tranquility and quiet and does not care about fairness.  
***

He is not bothered in the slightest by deviations from the norm or even extreme differences in lifestyle or appearance.  He is often sad and dejected.  He is rarely jealous.  He is grounded in reality.  He is somewhat fearful in the face of imminent danger.  He is quick to form negative views about things.  He doesn't mind a little tumult and discord in day-to-day living.  He likes to take it easy.  He likes to brawl, though he is disturbed by this since he values quiet so, at least in the abstract.  He tries to keep his things orderly.  He has a tendency to consider ideas and abstractions over practical applications.  He is somewhat quarrelsome, and he is bothered by this since he values friendship.  He does not often feel lustful.  He is not particularly interested in what others think of him.  He can occasionally lose focus on the matter at hand.  He finds helping others emotionally rewarding.  He  stiffens up when he's surprised.  He needs alcohol to get through the working day.  
***

Overall, Ustuth is untroubled by unmet needs.  He is not distracted after being unable to pray to Togal the Strike of Turquoise.  He is not distracted after being unable to pray to Aned the Jewels of Chancing.  He is not distracted after being unable to pray to Uker Moistensplashed the Oceanic Seal.  He is not distracted after being unable to pray to Ral the Diamond Guilds.  He is not distracted after being away from people.  He is not distracted after doing nothing creative.  He is not distracted after leading an unexciting life.  He is not distracted after being unable to acquire something.  He is not distracted after being kept from alcohol.  He is not distracted after a lack of decent meals.  He is not distracted after being unable to fight.  He is not distracted after a lack of trouble-making.  He is not distracted after being unable to argue. He is not distracted after being unable to be extravagant.  He is not distracted after not learning anything.  He is not distracted after being unable to help anybody.  He is not distracted after a lack of abstract thinking.  He is not distracted after being unoccupied.  He is not distracted after being unable to make merry.  He is not distracted after being unable to admire art.  He is not distracted after being unable to practice a craft.  He is not distracted after being away from family.  He is not distracted after being away from friends.  He is not distracted after being unable to practice a martial art.  He is not distracted after being unable to practice a skill.  He is not distracted after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

