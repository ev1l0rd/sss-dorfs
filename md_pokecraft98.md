###   `pokecraft98', Mason  
 
***

"Hard work is the true sign of character."Within the last season, he felt satisfied upon improving stone crafting.  He felt satisfied at work.  He felt satisfied at work.  He felt satisfied at work.  He was interested near his own fine Bed.  He felt euphoric due to inebriation.  He was annoyed after having a drink without using a goblet, cup or mug.  He was blissful after sleeping in a good bedroom.  He felt satisfied at work.  He was annoyed at the lack of chairs.  He felt love talking with father.  He felt satisfied at work.  He felt bitter after getting into an argument.  
***

He is the son of îton Oillull and Unib Fortresspower.  He is a dubious worshipper of Ral the Diamond Guilds, a worshipper of Aned the Jewels of Chancing, a casual worshipper of Datan and a worshipper of Togal the Strike of Turquoise.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He is a former member of The Safe Glaze.  He arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

He is eighty years old, born on the 7th of Galena in the year 432.  
***

His medium-length sideburns are neatly combed.  His very long moustache is neatly combed.  His very long beard is braided.  His hair is clean-shaven.  His ears are somewhat short.  His nose bridge is somewhat concave.  His ochre eyes are slightly rounded.  His hair is burnt sienna with a touch of gray.  His skin is sepia.  
***

He is very slow to tire and strong.  
***

`pokecraft98' likes sylvite, lead, red flash opal, clear glass, breastplates, thrones, the words of The Flutes of Bewildering and the sound of The Incense of Toning.  When possible, he prefers to consume whip wine.  He absolutely detests snails.  
***

He has a great affinity for language, very good creativity and a feel for music, but he has little willpower, poor spatial senses and quite poor focus.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally values hard work.  He dreams of creating a great work of art.  
***

He is unbelievably stubborn and will stick with even the most futile action once his mind is made up.  He does not generally respond to emotional appeals.  He has a very calm demeanor.  He takes offered help and gifts without feeling particularly grateful.  He has a tendency to go it alone, without considering the advice of others.  He tries to do things correctly each time.  He lives a fast-paced life.  He has a greedy streak.  He is quite comfortable with others that have a different appearance or culture.  He is moved by art and natural beauty, and he is troubled by this since he dislikes the natural world.  He can handle stress.  He  is currently more rude.  He  is currently more fearless.  He  is currently more confident.  He  is currently more shameless.  He  is currently less private.  He  is currently more thoughtless.  He  rolls his eyes when he's annoyed.  He needs alcohol to get through the working day.  
***

Overall, Id is somewhat focused with satisfied needs.  He is unfocused after being unable to pray to Aned the Jewels of Chancing.  He is unfocused after being unable to pray to Togal the Strike of Turquoise.  He is not distracted after being away from people.  He is unfettered after staying occupied.  He is unfettered after doing something creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is level-headed after drinking.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is not distracted after a lack of trouble-making.  He is not distracted after being unable to argue.  He is unfocused after being unable to be extravagant.  He is unfettered after learning something.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is level-headed after admiring art.  He is unfettered after practicing a craft.  He is not distracted after being away from family.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is unfettered after practicing a skill.  He is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

