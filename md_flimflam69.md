###   `FlimFlam69', Drunken Shitfuck  
 
***

"I'm fine."Within the last season, he felt pleasure near his own fine Bed.  He felt bitter after getting into an argument.  He felt euphoric due to inebriation.  He felt tenderness talking with a child.  He was annoyed after having a drink without using a goblet, cup or mug.  He felt tenderness talking with the spouse.  He felt pleasure near a fine Door.  He was annoyed after sleeping on a rough cave floor.  He was annoyed at the lack of chairs.  He felt satisfied at work.  
***

He is married to `junko' and has 7 children:  Aban Blockadesaves, Ustuth Stockadebraided, Udib Gatepaints, `Lily', Eshtân Coalfoot, Endok Smiththeaters and Besmar Jadechannel.  He is the son of Erib Subtleracks and Zas Claspveils.  He is a faithful worshipper of Ral the Diamond Guilds, a worshipper of Aned the Jewels of Chancing, a faithful worshipper of Uker Moistensplashed the Oceanic Seal, a worshipper of Datan, a casual worshipper of Togal the Strike of Turquoise and a faithful worshipper of The Chances of Coincidence.  He is an apprentice under Snodub Hobbledmaligned.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He is a former member of The Vault of Tempests.  He is a former member of The Sorcery of Whipping.  He is a former member of The League of Fruits.  He arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

He is seventy-six years old, born on the 10th of Slate in the year 436.  
***

He has a very narrow chin.  His hair is extremely dense.  His very long sideburns are neatly combed.  His very long moustache is arranged in double braids.  His very long beard is braided.  His medium-length hair is braided.  He has a scratchy voice.  His teeth are gapped.  His head is short.  His ears are free-lobed.  His hair is burnt sienna.  His skin is sepia.  His eyes are ochre.  
***

He is incredibly tough, very rarely sick and quick to heal.  
***

`FlimFlam69' likes hornblende, sterling silver, cinnamon grossular, paradise nut wood wood, giant ibex hoof, alpaca wool, the color clear, bucklers, cabinets, yellow bullheads for their whiskers, the words of The South Ways and the sight of The Silk of Luxury.  When possible, he prefers to consume sun berries, bayberry wine, hempseed oil and dwarven wheat flour.  He absolutely detests cave spiders.  
***

He has a very good sense of empathy and good intuition, but he has poor analytical abilities and a poor memory.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, really respects those that take the time to master a skill, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally finds artwork boring and doesn't really see the point of working hard.  He dreams of raising a family.  
***

He is deliberately cruel to those unfortunate enough to be subject to his sadism.  He is grateful when others help him out and tries to return favors.  He finds helping others emotionally rewarding.  He could be considered rude.  He  is currently more rude.  He tends to be a little tight with resources when working on projects.  He tends to hang on to grievances.  He tends to make a small mess with his own possessions.  He has a noticeable lack of perseverance.  He is very humble.  He has a greedy streak.  He generally acts with a narrow focus on the current activity.  He finds obligations confining, though he is conflicted by this for more than one reason.  He  is currently more fearless.  He  is currently more confident.  He  is currently more shameless.  He  is currently less private.  He  is currently more thoughtless.  He often lowers his eyes when he's thinking.  He scratches his ear whenever he's bored.  He needs alcohol to get through the working day.  
***

Overall, Rith is unfocused by unmet needs.  He is level-headed after spending time with people.  He is not distracted after being unoccupied.  He is not distracted after doing nothing creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is level-headed after drinking.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is unfettered after causing trouble.  He is unfettered after arguing.  He is unfocused after being unable to be extravagant.  He is unfocused after not learning anything.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is not distracted after being unable to practice a craft.  He is level-headed after being with family.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is not distracted after being unable to practice a skill.  He is unfocused after being unable to take it easy.  He is unfocused after being unable to pray to Ral the Diamond Guilds.  He is unfocused after being unable to pray to Aned the Jewels of Chancing.  He is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  He is unfocused after being unable to pray to Datan.  He is unfocused after being unable to pray to The Chances of Coincidence.  
***

A short, sturdy creature fond of drink and industry. 
***

