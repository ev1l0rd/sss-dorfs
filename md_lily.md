###   `Lily', Fishery Worker  
 
***

"I don't feel like telling you about it."Within the last season, she felt love talking with father.  She felt euphoric due to inebriation.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt satisfied at work.  She was annoyed after sleeping on a rough cave floor.  She felt bitter after getting into an argument.  She was annoyed at the lack of chairs.  She felt tenderness talking with the spouse.  She felt love talking with mother.  She felt pleasure after a satisfying acquisition.  She was content after putting on a superior item.  
***

She is married to `Aurora Wright'.  She is the daughter of `junko' and `FlimFlam69'.  She is a worshipper of Togal the Strike of Turquoise, a worshipper of Datan, a worshipper of Aned the Jewels of Chancing, a faithful worshipper of Ral the Diamond Guilds, a worshipper of Uker Moistensplashed the Oceanic Seal, a worshipper of The Chances of Coincidence and a casual worshipper of Ipa Fordlark the Mountainous Bear.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Vault of Tempests.  She arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

She is thirty-one years old, born on the 25th of Obsidian in the year 481.  
***

Her extremely narrow ears are splayed out.  Her very short hair is neatly combed.  Her eyebrows are extremely short.  Her hair is burnt sienna.  Her skin is sepia.  Her eyes are ochre.  
***

She is quick to tire.  
***

`Lily' likes periclase, lead, yellow grossular, crundle ivory, coral, gems, shields, barrels, cows for their haunting moos and the sight of The Euphoric Sheen.  When possible, she prefers to consume guppy and banana beer.  She absolutely detests jumping spiders.  
***

She has good creativity and willpower, but she has a questionable spatial sense.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally values sacrifice, finds the ideas of independence and freedom somewhat foolish and doesn't care if others take the time to master skills.  She dreams of creating a great work of art.  
***

She is extremely confident of herself in situations requiring her skills.  She  is currently more confident.  She does not generally respond to emotional appeals. She is a friendly individual.  She doesn't cling tightly to ideas and is open to changing her mind.  She has a sense of duty.  She is pleased by her own appearance and talents.  She has a tendency to go it alone, without considering the advice of others.  She is quick to form negative views about things.  She has a greedy streak.  She is quite ambitious.  She  is currently more rude.  She  is currently more fearless.  She  is currently more shameless.  She  is currently less private.  She  is currently more thoughtless.  She winks during conversations. She idly chews her lips when she's bored.  She  talks to inanimate objects when there is a lull in the conversation.  She needs alcohol to get through the working day.  
***

Overall, Minkot is unfocused by unmet needs.  She is unfocused after being unable to pray to Togal the Strike of Turquoise.  She is unfocused after being unable to pray to Datan.  She is unfocused after being unable to pray to Aned the Jewels of Chancing.  She is unfocused after being unable to pray to Ral the Diamond Guilds.  She is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  She is unfocused after being unable to pray to The Chances of Coincidence.  She is level-headed after spending time with people.  She is untroubled after staying occupied.  She is untroubled after doing something creative.  She is unfocused after leading an unexciting life.  She is unfocused after being unable to acquire something.  She is level-headed after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after being unable to fight.  She is not distracted after a lack of trouble-making.  She is unfettered after being extravagant.  She is unfocused after not learning anything.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfocused after being unable to admire art.  She is untroubled after practicing a craft.  She is level-headed after being with family.  She is unfocused after being away from friends.  She is unfocused after being unable to practice a martial art.  She is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

