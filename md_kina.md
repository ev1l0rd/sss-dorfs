###   `Kina', Mason  
 
***

"What's this about proper living?"Within the last season, he didn't feel anything due to inebriation.  He was annoyed after having a drink without using a goblet, cup or mug.  He was blissful after sleeping in a good bedroom.  He felt satisfied at work.  He felt satisfied upon improving masonry.  He didn't feel anything due to inebriation.  He was annoyed at the lack of chairs.  He felt satisfied at work.  He was annoyed after sleeping on a rough cave floor.  He felt fondness talking with a friend.  
***

He is a worshipper of Togal the Strike of Turquoise.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

He is eighty-eight years old, born on the 1st of Moonstone in the year 424.  
***

His very long sideburns are neatly combed.  His long moustache is neatly combed.  His medium-length beard is arranged in double braids.  His very long hair is tied in a pony tail.  His ears are splayed out.  He has a scratchy voice.  His lips are thick.  He has a narrow chin.  His eyelashes are quite long.  His sepia skin is wrinkled.  His head is somewhat tall.  His hair is burnt sienna with flecks of gray.  His eyes are ochre.  His eyes are slightly rounded.  
***

He is very rarely sick, but he is flimsy.  
***

`Kina' likes native gold, lead, sunstone, papyrus, gauntlets, bins, bracelets and the sound of The Fancy Periwinkle.  When possible, he prefers to consume grasshopper, blackberry wine and teff flour.  He absolutely detests snails.  
***

He has a great feel for social relationships, very good creativity and a natural inclination toward language, but he has quite poor focus and a poor kinesthetic sense.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, sees friendship as one of the finer things in life, believes that honesty is a high ideal, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce and finds nature somewhat disturbing.  He personally values artwork, sees perseverance in the face of adversity as bull-headed and foolish, values family and doesn't see the attainment of knowledge as important.  He dreams of crafting a masterwork someday.  
***

He is prone to hatreds and often develops negative feelings.  He feels strong urges and seeks short-term rewards.  He has a calm demeanor.  He is rarely happy or enthusiastic, and he is conflicted by this as he values parties and merrymaking in the abstract.  He often acts with compassion.  He tends to consider what others think of him.  He  is currently more shameless.  He has a sense of duty.  He has a greedy streak.  He thinks he is fairly important in the grand scheme of things.  He isn't particularly ambitious. He  is currently more rude.  He  is currently more fearless.  He  is currently more confident.  He  is currently less private.  He  is currently more thoughtless.  He  laughs loudly when he's excited.  He needs alcohol to get through the working day.  He likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, ònul is unfocused by unmet needs.  He is not distracted after being away from people.  He is not distracted after being unoccupied.  He is not distracted after doing nothing creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is not distracted after being kept from alcohol.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is unfocused after a lack of trouble-making.  He is unfocused after being unable to argue.  He is unfocused after being unable to be extravagant.  He is not distracted after not learning anything.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is unfocused after being unable to admire art.  He is not distracted after being unable to practice a craft.  He is unfocused after being away from family.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is not distracted after being unable to practice a skill.  He is unfocused after being unable to take it easy.  He is unfocused after being unable to pray to Togal the Strike of Turquoise.  
***

A short, sturdy creature fond of drink and industry. 
***

