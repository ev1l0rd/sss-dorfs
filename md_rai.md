###   `rai', Carpenter  
 
***

"It's best to slow down and just relax."Within the last season, he felt euphoric due to inebriation.  He was blissful after sleeping in a good bedroom.  He was irritated when drowsy.  He was annoyed after having a drink without using a goblet, cup or mug.  He was annoyed when caught in the rain.  He felt euphoric due to inebriation.  He was annoyed at the lack of chairs.  He felt satisfied at work.  He felt satisfied at work.  He didn't feel anything talking with a friend.  He was annoyed after sleeping on a rough cave floor.  
***

He is a casual worshipper of Togal the Strike of Turquoise.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

He is seventy-six years old, born on the 8th of Granite in the year 436.  
***

His hair is straight.  His short sideburns are neatly combed.  His very long moustache is arranged in double braids.  His very long beard is arranged in double braids.  His very long hair is braided.  He has a scratchy voice.  He has a deeply recessed chin.  His lips are thick.  His round ochre eyes are wide-set.  His ears are somewhat short.  His nose is slightly hooked.  His hair is burnt sienna.  His skin is sepia.  
***

He is quick to tire.  
***

`rai' likes phyllite, bismuth bronze, wax opal, backpacks, animal traps and jumping spider men for their striking appearance.  When possible, he prefers to consume lungfish, cherry wine and Longland flour.  He absolutely detests large roaches.  
***

He has a great deal of patience and a good spatial sense, but he has poor analytical abilities, a poor ability to manage or understand social relationships and next to no willpower.  
***

Like others in his culture, he has a great deal of respect for the law, greatly prizes loyalty, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally views decorum as a high ideal and is deeply offended by those that fail to maintain it, considers craftsdwarfship to be relatively worthless, doesn't care if others take the time to master skills and does not care about family one way or the other.  He dreams of creating a great work of art.  
***

He is prone to strong feelings of lust.  He finds the humor in most situations.  He doesn't try to get things done perfectly.  He does not easily fall in love and rarely develops positive sentiments.  He is quite polite.  He is currently more rude.  He thinks he is fairly important in the grand scheme of things.  He can handle stress.  He tends to make a small mess with his own possessions.  He occasionally overindulges.  He  is currently more fearless.  He  is currently more confident.  He  is currently more shameless.  He  is currently less private.  He  is currently more thoughtless.  He scratches his nose when he's nervous.  When he's trying to remember something, he usually starts tapping his feet.  He needs alcohol to get through the working day.  He likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, Stukos is unfocused by unmet needs.  He is not distracted after being away from people.  He is not distracted after being unoccupied.  He is not distracted after doing nothing creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is untroubled after drinking.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is unfocused after a lack of trouble-making.  He is unfocused after being unable to argue.  He is unfocused after being unable to be extravagant.  He is unfocused after not learning anything.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is unfocused after being unable to admire art.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

