###   `astronautlevel', manager  
 
***

"Everything's fine."Within the last season, she was annoyed at the lack of chairs.  She felt satisfied at work.  She felt satisfied at work.  She felt euphoric due to inebriation.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt satisfied at work.  She was blissful after sleeping in a good bedroom.  She felt satisfied upon improving mining.  She felt euphoric due to inebriation.  She was annoyed after sleeping on a rough cave floor.  She felt satisfied at work.  
***

She is a faithful worshipper of Togal the Strike of Turquoise.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is the manager of The Torch of Evening.  She arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

She is sixty-seven years old, born on the 15th of Galena in the year 445.  
***

She has a grating, raspy voice.  Her splayed out short ears have large hanging lobes.  Her hair is clean-shaven.  Her lips are thick.  Her slightly rounded ochre eyes are slightly wide-set.  Her ears are broad.  Her eyelashes are quite long.  Her hair is burnt sienna.  Her skin is sepia.  
***

She is very slow to tire and strong.  
***

`astronautlevel' likes native copper, pig iron, star sapphire, giant ocelot bone, cows for their haunting moos, harpies for their fearsome talons and the words of The Flutes of Bewildering.  When possible, she prefers to consume opossum, clownfish and pendant amaranth beer.  She absolutely detests hamsters.  
***

She has good creativity and a sum of patience, but she has bad intuition, a little difficulty with words and quite poor focus.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally values decorum, dignity and proper behavior.  She dreams of crafting a masterwork someday.  
***

She is bored by reality and would rather disappear utterly and forever into a world of made-up fantasy.  She has an overinflated sense of self-worth.  She is unfriendly and disagreeable, and she is bothered by this since she values friendship.  She doesn't stick with things if even minor difficulties arise.  She lives an orderly life, organized and neat.  She is an optimist.  She finds obligations confining, though she is conflicted by this for more than one reason.  She can sometimes act without deliberation.  She  is currently more thoughtless.  She generally acts with a narrow focus on the current activity.  She prefers that everyone live as harmoniously as possible.  She finds helping others emotionally rewarding.  She generally acts impartially and is rarely moved to mercy.  She is not particularly interested in what others think of her.  She  is currently more shameless. She  is currently more rude.  She  is currently more fearless.  She  is currently more confident.  She  is currently less private.  Her hands are animated when she speaks.  She needs alcohol to get through the working day.  She likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, Edëm is unfocused by unmet needs.  She is unfocused after being away from people.  She is unfettered after staying occupied.  She is unfocused after doing nothing creative.  She is unfocused after leading an unexciting life.  She is unfocused after being unable to acquire something.  She is unfettered after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after being unable to fight.  She is unfocused after being unable to argue.  She is unfocused after being unable to be extravagant.  She is not distracted after not learning anything.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfocused after being unable to admire art.  She is unfocused after being unable to practice a craft.  She is unfocused after being away from family.  She is unfocused after being away from friends.  She is unfocused after being unable to practice a martial art.  She is unfettered after practicing a skill.  She is unfocused after being unable to take it easy.  She is unfocused after being unable to pray to Togal the Strike of Turquoise.  
***

A short, sturdy creature fond of drink and industry. 
***

