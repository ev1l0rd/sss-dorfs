###   `Inori Shikuza', Shearer  
 
***

"What's this about proper living?"Within the last season, he felt bitter after getting into an argument.  He felt euphoric due to inebriation.  He was annoyed after having a drink without using a goblet, cup or mug.  He was annoyed after sleeping on a rough cave floor.  He was annoyed at the lack of chairs.  He felt satisfied at work.  He was interested near a fine Door.  
***

He is married to Åblel Roofprison.  He is the son of Tekkud Cudgelskins and Mafol Shellabbey.  He is a worshipper of Togal the Strike of Turquoise, a worshipper of The Chances of Coincidence, an ardent worshipper of Uker Moistensplashed the Oceanic Seal, a worshipper of Aned the Jewels of Chancing, a worshipper of Datan, a dubious worshipper of Ipa Fordlark the Mountainous Bear and a worshipper of Ral the Diamond Guilds.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He is a former member of The Flag of Mornings.  He is a former member of The Girder of Destiny.  He arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

He is twenty-eight years old, born on the 11th of Sandstone in the year 484.  
***

He is tall.  His short sideburns are neatly combed.  His very long moustache is neatly combed.  His very long beard is arranged in double braids.  His hair is clean-shaven.  His sepia skin is wrinkled.  His slightly protruding ochre eyes are slightly rounded.  His hair is burnt sienna.  
***

He is very agile, but he is flimsy and quite susceptible to disease.  
***

`Inori Shikuza' likes siltstone, silver, aquamarine, crystal glass, kangaroo leather, water buffalo hoof, coral, sheep wool, the color cardinal, war hammers, bucklers, grates, goats for their eating habits and the sound of The Incense of Toning.  When possible, he prefers to consume coyote and rambutan wine.  He absolutely detests oysters.  
***

He has good intuition, willpower and a good kinesthetic sense, but he has a shortage of patience.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally sees war as a useful means to an end and sees guile and cunning as indirect and somewhat worthless.  He dreams of creating a great work of art.  
***

He is a pessimist.  He often acts with compassion.  He tends to consider what others think of him. He  is currently more shameless.  He tends to ask others for help with difficult decisions.  He is brave in the face of imminent danger.  He  is currently more fearless.  He tries to keep his things orderly.  He often feels discouraged.  He tends to avoid crowds.  He tends to be a little wasteful when working on projects.  He generally acts with a narrow focus on the current activity.  He is quite polite.  He  is currently more rude.  He  is currently more confident.  He  is currently less private.  He  is currently more thoughtless.  He speaks very deliberately when he's annoyed.  He needs alcohol to get through the working day.  
***

Overall, Likot is unfocused by unmet needs.  He is unfocused after being unable to pray to Togal the Strike of Turquoise.  He is unfocused after being unable to pray to The Chances of Coincidence.  He is distracted after being unable to pray to Uker Moistensplashed the Oceanic Seal.  He is unfocused after being unable to pray to Aned the Jewels of Chancing.  He is unfocused after being unable to pray to Datan.  He is unfocused after being unable to pray to Ral the Diamond Guilds.  He is not distracted after being unoccupied.  He is unfocused after doing nothing creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is level-headed after drinking.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is unfettered after causing trouble.  He is unfettered after arguing.  He is unfocused after being unable to be extravagant.  He is unfocused after not learning anything.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is unfocused after being unable to admire art.  He is unfocused after being unable to practice a craft.  He is unfocused after being away from family.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is not distracted after being unable to practice a skill.  He is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

